@extends('layout.table')
@section('title')
Halaman Detail cast
@endsection
@section('content')

<h1 class="text-primary">{{$cast->nama}}</h1>
<p>{{$cast->bio}}</p>

<a href="/cast" class="btn btn-secondary my-2">Kembali</a>

@endsection