<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController; 
use App\Http\Controllers\AuthController; 
use App\Http\Controllers\castController; 

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'Home']);
Route::get('/register', [AuthController::class, 'Daftar']);
Route::post('/Welcome', [AuthController::class, 'Welcome']);

Route::get('/table', function(){
    return view('layout.table');
});

Route::get('/data-tables', function(){
    return view('halaman.data-tables');
});

Route::get('/Table', function(){
    return view('halaman.Table');
});

//CRUD cast

//Create Data
//Route untuk mengarah ke form tambah cast
Route::get('/cast/create', [castController::class, 'create']);
//Route untuk menyimpan data inputan ke db
Route::post('/cast', [castController::class, 'store']);

//Read
//menampilkan semua data yang ada db
Route::get('/cast', [castController::class, 'index']);
//Detail cast berdasarkan ID
Route::get('/cast/{cast_id}', [castController::class, 'show']);

//update
//route yang mengarah ke form update berdasarkan id
Route::get('/cast/{cast_id}/edit', [castController::class, 'edit']);
//untuk update data berdasarkan id di db
Route::put('/cast/{cast_id}', [castController::class, 'update']);

//Delete
Route::delete('/cast/{cast_id}', [castController::class, 'destroy']);



