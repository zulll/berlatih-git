<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController; 
use App\Http\Controllers\AuthController; 

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'Home']);
Route::get('/register', [AuthController::class, 'Daftar']);
Route::post('/Welcome', [AuthController::class, 'Welcome']);

Route::get('/table', function(){
    return view('layout.table');
});

Route::get('/data-tables', function(){
    return view('halaman.data-tables');
});

Route::get('/Table', function(){
    return view('halaman.Table');
});