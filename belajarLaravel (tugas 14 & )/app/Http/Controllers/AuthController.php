<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function Daftar()
    {
        return view('halaman.auth'); 
    }

    public function Welcome(Request $request)
    {
        $Firstname = $request['first'];
        $Lastname = $request['last'];

        return view('halaman.datang', ['Firstname' => $Firstname , 'Lastname' => $Lastname]);
    }
}
